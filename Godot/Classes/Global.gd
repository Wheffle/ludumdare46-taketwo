extends Node


enum SCENE {
	MENU,
	TABLETOP
}


enum MOOD {
	HAPPY,
	SAD,
	SICK,
	ASLEEP,
	DEAD
}
