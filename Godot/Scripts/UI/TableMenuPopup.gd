extends Control


signal quit
signal back


func _on_QuitButton_pressed():
	emit_signal("quit")


func _on_ContinueButton_pressed():
	emit_signal("back")
