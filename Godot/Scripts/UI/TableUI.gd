extends Control


onready var score_label = $MarginContainer/HBoxContainer/MarginContainer/Label
onready var hint_label = $MarginContainer/HBoxContainer/MarginContainer2/Label
onready var death_count_container = $MarginContainer/HBoxContainer/HBoxContainer


func set_score(new_score : int) -> void:
	score_label.text = "$" + String(new_score)


func set_deaths(deaths : int) -> void:
	for i in range(death_count_container.get_child_count()):
		var child = death_count_container.get_child(i)
		if i < deaths:
			child.visible = true
		else:
			child.visible = false


func _on_CashHintTimer_timeout():
	hint_label.visible = false

