extends Control


signal finish_session

onready var score_label = $CenterContainer/NinePatchRect/MarginContainer/MarginContainer/CenterContainer/VBoxContainer/Label2


func set_score(amount : int) -> void:
	score_label.text = "Your small business made $" + String(amount)


func _on_TextureButton_pressed():
	emit_signal("finish_session")
