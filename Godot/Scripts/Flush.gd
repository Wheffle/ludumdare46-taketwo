extends Node2D


onready var sprite = $Sprite
onready var timer = $Timer


var step_offset := Vector2(-4, 0)
var num_steps : int = 4
var step : int = 0
var flushing : bool = false


func start_flush() -> void:
	if !flushing:
		flushing = true
		sprite.position = Vector2(0, 0)
		sprite.visible = true
		timer.start(0.25)
		yield(timer, "timeout")
		for i in range(num_steps):
			sprite.translate(step_offset)
			timer.start(0.25)
			yield(timer, "timeout")
		sprite.visible = false
		flushing = false

