extends Area2D

class_name Device


signal death

onready var omiga = $Omiga
onready var hunger_icon = $HungerIcon
onready var dead_icon = $DeadIcon
onready var illness_icon = $IllnessIcon
onready var keychain = $KeychainPoint/Keychain
onready var soundwave = $Soundwave
onready var update_timer = $UpdateTimer
onready var alert_timer = $AlertTimer
onready var alarm_audio = $AudioAlarmPlayer

var paused : bool = false setget _set_paused

var velocity := Vector2()
var drag : float = 130


#func _process(delta):
#	$HungerTest.text = "H: " + String(omiga.hunger)
#	$BowelTest.text = "B: " + String(omiga.bowels)
#	$IllnessTest.text = "I: " + String(omiga.illness)


func _physics_process(delta):
	if !paused and velocity != Vector2(0, 0):
		translate(velocity * delta)
		var new_magnitude : float = velocity.length() - (drag * delta)
		if new_magnitude <= 0:
			velocity = Vector2(0, 0)
		else:
			velocity = Vector2(new_magnitude, 0).rotated(velocity.angle())


func _set_paused(is_paused : bool) -> void:
	paused = is_paused
	update_timer.paused = is_paused
	alert_timer.paused = is_paused
	omiga.paused = is_paused


func _alarm() -> void:
	if !alarm_audio.playing:
		alarm_audio.play()
	soundwave.visible = true
	alert_timer.start(3)


func _on_FeedButton_pressed():
	if !paused:
		omiga.give_food()
		_refresh_icons()


func _on_FlushButton_pressed():
	if !paused:
		omiga.flush_poop()


func _on_PillButton_pressed():
	if !paused:
		omiga.give_medicine()
		_refresh_icons()


func _refresh_icons() -> void:
	if omiga.hunger >= 0.5 and !hunger_icon.visible:
		hunger_icon.visible = true
		_alarm()
	elif omiga.hunger < 0.5 and hunger_icon.visible:
		hunger_icon.visible = false
	if omiga.illness >= 0.5 and !illness_icon.visible:
		illness_icon.visible = true
		_alarm()
	elif omiga.illness < 0.5 and illness_icon.visible:
		illness_icon.visible = false
	if omiga.dead and !dead_icon.visible:
		dead_icon.visible
		_alarm()

func _on_UpdateTimer_timeout():
	_refresh_icons()
	if omiga.dead:
		$UpdateTimer.stop()


func _on_AlertTimer_timeout():
	soundwave.visible = false


func _on_Omiga_death():
	dead_icon.visible = true
	emit_signal("death")



func _on_OmigaDevice_input_event(viewport, event, shape_idx):
	pass # Replace with function body.
