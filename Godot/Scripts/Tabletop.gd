extends Node2D


const BasicDevice = preload("res://Scenes/OmigaDevice.tscn")
const YellowDevice = preload("res://Scenes/Omigas/YellowOmigaDevice.tscn")
const WhiteDevice = preload("res://Scenes/Omigas/WhiteOmigaDevice.tscn")
const PinkDevice = preload("res://Scenes/Omigas/PinkOmigaDevice.tscn")
const BlackDevice = preload("res://Scenes/Omigas/BlackOmigaDevice.tscn")

const device_spawn_offset : float = 256.00
const device_spawn_velocity : float = 350.00

onready var cursor = $EasyCursor
onready var spawn_timer = $SpawnTimer
onready var devices = $Devices
onready var ui = $TableUI
onready var payday_timer = $PaydayTimer
onready var loss_ui = $TableLossPopup
onready var menu_ui = $TableMenuPopup

var score : int = 0
var deaths : int = 0

var base_spawn_time : float = 25
var max_deaths : int = 3


func _ready() -> void:
	spawn_timer.start(0.5)


func _process(delta : float) -> void:
	if Input.is_action_just_pressed("ui_cancel") and !menu_ui.visible and !loss_ui.visible:
		pause()
		cursor.reset_grab()
		menu_ui.visible = true


func add_device(DeviceScene : PackedScene) -> void:
	var device = DeviceScene.instance()
	var dice = randi() % 4
	var initial_speed = device_spawn_velocity + rand_range(0, 25)
	match(dice):
		0: #TOP
			device.position.y = -device_spawn_offset
			device.position.x = round(rand_range(device_spawn_offset, 1280-device_spawn_offset))
			device.velocity = Vector2(0, initial_speed)
		1: #BOTTOM
			device.position.y = 720 + device_spawn_offset
			device.position.x = round(rand_range(device_spawn_offset, 1280-device_spawn_offset))
			device.velocity = Vector2(0, -initial_speed)
		2: #LEFT
			device.position.x = -device_spawn_offset
			device.position.y = round(rand_range(device_spawn_offset, 720-device_spawn_offset))
			device.velocity = Vector2(initial_speed, 0)
		3: #RIGHT
			device.position.x = 1280 + device_spawn_offset
			device.position.y = round(rand_range(device_spawn_offset, 720-device_spawn_offset))
			device.velocity = Vector2(-initial_speed, 0)
	devices.add_child(device)
	device.keychain.push()
	device.connect("death", self, "_on_omiga_death")


func _on_SpawnTimer_timeout():
	var device_count = devices.get_child_count()
	var candidates := []
	if device_count < 2:
		candidates.append(BasicDevice)
	elif device_count == 2:
		candidates.append(YellowDevice)
	elif device_count == 3:
		candidates.append(BasicDevice)
	elif device_count == 4:
		candidates.append(WhiteDevice)
	elif device_count == 5:
		candidates.append(BasicDevice)
	elif device_count == 6:
		candidates.append(PinkDevice)
	elif device_count < 10:
		candidates.append(BasicDevice)
		candidates.append(YellowDevice)
		candidates.append(WhiteDevice)
		candidates.append(PinkDevice)
	elif device_count == 10:
		candidates.append(BlackDevice)
	elif device_count < 12:
		candidates.append(BasicDevice)
		candidates.append(YellowDevice)
		candidates.append(WhiteDevice)
		candidates.append(PinkDevice)
	else:
		candidates.append(BasicDevice)
		candidates.append(YellowDevice)
		candidates.append(WhiteDevice)
		candidates.append(PinkDevice)
		candidates.append(BlackDevice)
	var dice = randi() % candidates.size()
	add_device(candidates[dice])
	spawn_timer.start(base_spawn_time + device_count)


func _on_PaydayTimer_timeout():
	score += devices.get_child_count()
	score -= deaths
	ui.set_score(score)


func pause() -> void:
	spawn_timer.paused = true
	payday_timer.paused = true
	for child in devices.get_children():
		child.paused = true


func unpause() -> void:
	spawn_timer.paused = false
	payday_timer.paused = false
	for child in devices.get_children():
		child.paused = false


func _on_omiga_death() -> void:
	deaths += 1
	ui.set_deaths(deaths)
	if deaths >= max_deaths:
		pause()
		loss_ui.set_score(score)
		loss_ui.visible = true


func _on_TableLossPopup2_back():
	if menu_ui.visible:
		unpause()
		menu_ui.visible = false


func _on_TableLossPopup2_quit():
	if menu_ui.visible == true:
		get_parent().change_scene(Global.SCENE.MENU)


func _on_TableLossPopup_finish_session():
	if loss_ui.visible == true:
		get_parent().session_score = score
		get_parent().change_scene(Global.SCENE.MENU)
