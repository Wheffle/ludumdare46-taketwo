extends Node2D


var push_impulse : float = 5
var rotation_drag : float = 5
var rotation_speed : float = 0


func _physics_process(delta):
	rotate(rotation_speed * delta)
	var drag_step = rotation_drag * delta
	if abs(rotation_speed) <= drag_step:
		rotation_speed = 0
	elif rotation_speed > 0:
		rotation_speed -= drag_step
	elif rotation_speed < 0:
		rotation_speed += drag_step


func push() -> void:
	var push_magnitude : float = rand_range(push_impulse / 2, push_impulse)
	var push_dir = 1
	if randi() % 2 == 0:
		push_dir = -1
	rotation_speed = clamp(rotation_speed + (push_magnitude * push_dir), -push_impulse, push_impulse)
