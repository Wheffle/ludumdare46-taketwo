extends Area2D

class_name Button2D


signal pressed

export var audio_player_path : NodePath

onready var sprite = $Sprite
onready var timer = $Timer

var audio_player : AudioStreamPlayer2D


func _ready():
	audio_player = get_node(audio_player_path)

func press() -> void:
	sprite.frame = 1
	for child in get_children():
		if child is Node2D and child != sprite:
			child.position.y = 3
	_play_audio()
	timer.start(0.15)
	emit_signal("pressed")


func _play_audio():
	if audio_player != null and !audio_player.playing:
		audio_player.play()


func _on_Timer_timeout():
	sprite.frame = 0
	for child in get_children():
		if child is Node2D and child != sprite:
			child.position.y = 0
