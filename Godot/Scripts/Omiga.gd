extends Node2D


signal death

onready var creature = $Screen/Creature
onready var poop = $Screen/Poop
onready var flush = $Screen/Flush
onready var health_bar = $HealthBar2D

var health : float = 1.0
var hunger : float = 0.0
var illness : float = 0.0
var bowels : float = 0.0
var sleepy : float = 0.0

export var health_regen : float = 0.005
export var hunger_inc : float = 0.03
export var bowels_inc : float = 0.02

export var illness_chance : float = 0.01
export var illness_inc : float = 0.50
export var illness_health_loss : float = 0.01

export var food_hunger_decrease : float = 0.50
export var food_bowel_increase : float = 0.30
export var starve_damage_factor : float = 0.50
export var overfeed_damage_factor : float = 0.1
export var overfeed_illness_factor : float = 1.0
export var medicine_illness_decrease : float = 0.50
export var medicine_hunger_decrease : float = 0.25
export var medicine_hunger_decrease_without_illness : float = 0.75

export var illness_inc_per_poop : float = 0.01
export var illness_inc_per_poop_while_feeding : float = 0.10

export var sleepy_inc : float = 0.01
export var sleepy_dec_while_asleep : float = 0.03
export var sleep_state_change_chance_factor : float = 1.0
export var health_regen_while_asleep : float = 0.002
export var illness_health_loss_while_asleep : float = 0.002
export var illness_dec_while_asleep : float = 0.003

export var toilet_illness_inc : float = 0

var poops : int = 0
var asleep : bool = false
var dead : bool = false

onready var path := [
	creature.position,
	creature.position + Vector2(1, 1),
	creature.position + Vector2(2, 1),
	creature.position + Vector2(3, 0),
	creature.position + Vector2(4, 0),
	creature.position + Vector2(3, -1),
	creature.position + Vector2(2, -1),
	creature.position + Vector2(1, 0),
	creature.position + Vector2(0, 0),
	creature.position + Vector2(-1, 1),
	creature.position + Vector2(-2, 1),
	creature.position + Vector2(-3, 0),
	creature.position + Vector2(-4, 0),
	creature.position + Vector2(-3, -1),
	creature.position + Vector2(-2, -1),
	creature.position + Vector2(-1, 0)
]

var path_index : int = 0

var paused : bool = false setget _set_paused

func give_food() -> void:
	if !paused and !dead and !asleep and !creature.animating and !flush.flushing:
		_add_hunger(-food_hunger_decrease)
		_add_bowels(food_bowel_increase, false)
		_add_illness(illness_inc_per_poop_while_feeding * poops)
		creature.start_feed_animation()


func give_medicine() -> void:
	if !paused and !dead and !asleep and !creature.animating and !flush.flushing:
		if illness >= medicine_illness_decrease:
			_add_hunger(-medicine_hunger_decrease)
		else:
			_add_hunger(-medicine_hunger_decrease_without_illness)
		_add_illness(-medicine_illness_decrease)
		_add_illness(illness_inc_per_poop_while_feeding * poops)
		creature.start_med_animation()


func flush_poop() -> void:
	if !paused and !dead and !creature.animating and !flush.flushing:
		poops = 0
		poop.visible = false
		_add_illness(toilet_illness_inc)
		flush.start_flush()


func _add_health(amount : float) -> void:
	health = clamp(health+amount, 0, 1)
	health_bar.progress = health
	if health == 0 and !dead:
		dead = true
		emit_signal("death")


func _add_hunger(amount : float) -> void:
	hunger += amount
	if hunger > 1.0:
		var damage = 1.0 - hunger
		hunger = 1.0
		_add_health(damage * starve_damage_factor)
	elif hunger < 0.0:
		var damage = hunger
		_add_health(damage * overfeed_damage_factor)
		_add_illness(-damage * overfeed_illness_factor)
		hunger = 0.0


func _add_illness(amount : float) -> void:
	var prev_illness = illness
	illness = clamp(illness+amount, 0, 1)


func _add_bowels(amount : float, poop_immediately : bool = true) -> void:
	bowels += amount
	if bowels > 1.0 and poop_immediately:
		bowels -= 1.0
		_take_dump()


func _add_sleepy(amount : float) -> void:
	sleepy += amount
	if !asleep and sleepy > 1.0:
		var chance = (sleepy - 1.0) * sleep_state_change_chance_factor
		if randf() <= chance:
			sleepy = 1.0
			asleep = true
	elif asleep and sleepy < 0.0:
		var chance = abs(sleepy) * sleep_state_change_chance_factor
		if randf() <= chance:
			sleepy = 0.0
			asleep = false


func _take_dump() -> void:
	poops += 1
	poop.visible = true


func _refresh_mood() -> void:
	if dead:
		creature.set_mood(Global.MOOD.DEAD)
	elif asleep:
		creature.set_mood(Global.MOOD.ASLEEP)
	elif illness > 0.50:
		creature.set_mood(Global.MOOD.SICK)
	elif hunger >= 0.75:
		creature.set_mood(Global.MOOD.SAD)
	elif health < 0.5:
		creature.set_mood(Global.MOOD.SAD)
	else:
		creature.set_mood(Global.MOOD.HAPPY)


func _on_Timer_timeout():
	if !dead and !creature.animating:
		if asleep:
			_add_health(health_regen_while_asleep)
			_add_illness(-illness_dec_while_asleep)
			_add_illness(illness_inc_per_poop * poops)
			var illness_damage = illness_health_loss_while_asleep * illness
			if illness >= 0.75:
				illness_damage *= 2
			_add_health(-illness_damage)
			_add_sleepy(-sleepy_dec_while_asleep)
		else:
			path_index = (path_index + 1) % path.size()
			creature.position = path[path_index]
			_add_hunger(hunger_inc)
			_add_bowels(bowels_inc)
			if rand_range(0.0, 1.0) <= illness_chance:
				_add_illness(illness_inc)
			_add_illness(poops * illness_inc_per_poop)
			if hunger < 0.25:
				_add_health(health_regen)
			elif hunger >= 0.75:
				_add_health(-health_regen * 2)
			var illness_damage = illness_health_loss * illness
			if illness >= 0.75:
				illness_damage *= 3
			elif illness >= 0.50:
				illness_damage *= 2
			_add_health(-illness_damage)
			_add_sleepy(sleepy_inc)
		_refresh_mood()


func _set_paused(is_paused : bool) -> void:
	paused = is_paused
	creature.paused = is_paused
	$Timer.paused = is_paused

