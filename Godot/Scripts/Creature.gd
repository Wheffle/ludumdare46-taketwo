extends Node2D

class_name Creature


signal animation_finished

onready var sprite = $Sprite
onready var food_sprite = $FoodSprite
onready var pill_sprite = $PillSprite
onready var z_sprite = $ZSprite
onready var timer = $Timer

var idle_frame : int = 0
var animating : bool = false
var animation_stage : int = 0
var animation_length : int = 5

var paused : bool = false setget _set_paused


func set_mood(mood : int) -> void:
	z_sprite.visible = false
	match(mood):
		Global.MOOD.HAPPY:
			idle_frame = 0
		Global.MOOD.SAD:
			idle_frame = 1
		Global.MOOD.SICK:
			idle_frame = 3
		Global.MOOD.ASLEEP:
			idle_frame = 3
			z_sprite.visible = true
		Global.MOOD.DEAD:
			idle_frame = 4
	if !animating:
		sprite.frame = idle_frame


func start_feed_animation() -> void:
	_start_eat_animation(food_sprite)


func start_med_animation() -> void:
	_start_eat_animation(pill_sprite)


func _start_eat_animation(target_sprite : Sprite) -> void:
	if !animating:
		animating = true
		animation_stage = 0
		target_sprite.visible = true
		target_sprite.frame = 0
		sprite.frame = 2
		timer.start(0.5)
		yield(timer, "timeout")
		while animation_stage < animation_length:
			animation_stage += 1
			if animation_stage % 2 == 0:
				sprite.frame = 2
			else:
				sprite.frame = idle_frame
				if target_sprite.frame < food_sprite.hframes-1:
					target_sprite.frame += 1
				else:
					target_sprite.visible = false
			timer.start(0.5)
			yield(timer, "timeout")
		sprite.frame = idle_frame
		animating = false
		emit_signal("animation_finished")


func _set_paused(is_paused : bool) -> void:
	paused = is_paused
	timer.paused = is_paused

