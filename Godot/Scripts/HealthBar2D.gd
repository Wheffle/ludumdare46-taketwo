extends Node2D


onready var bar = $Polygon2D

onready var base_x : float = -16
onready var length : float = 32

var progress : float = 1.0 setget _set_progress


func _set_progress(new_progress) -> void:
	progress = clamp(new_progress, 0, 1)
	bar.polygon[2].x = base_x + floor(length * progress)
	bar.polygon[3].x = base_x + floor(length * progress)

