extends Area2D


var sliding_device : bool = false
var grabbed_device : Device = null
var grabbed_offset : Vector2 = Vector2()
var clicked : bool = false


func _physics_process(delta : float) -> void:
	if clicked:
		clicked = false
		_process_click()
	if Input.is_action_just_pressed("mouse_left_click") and !sliding_device:
		clicked = true
		global_position = get_global_mouse_position()
	elif Input.is_action_just_released("mouse_left_click") and sliding_device:
		reset_grab()
	if sliding_device:
		_slide_device()


func _slide_device() -> void:
	var destination : Vector2 = get_global_mouse_position() + grabbed_offset
	var translation : Vector2 = destination - grabbed_device.global_position
	var attenuated_translation = translation / 8
	grabbed_device.translate(attenuated_translation)
	if grabbed_device.global_position.x < 0:
		grabbed_device.global_position.x = 0
	elif grabbed_device.global_position.x >= 1280:
		grabbed_device.global_position.x = 1279
	if grabbed_device.global_position.y < 0:
		grabbed_device.global_position.y = 0
	if grabbed_device.global_position.y >= 720:
		grabbed_device.global_position.y = 719


func _process_click() -> void:
	var areas = get_overlapping_areas()
	for i in range(areas.size()-1, -1, -1):
		var area = areas[i]
		if !sliding_device and area is Device and !area.paused:
			grabbed_device = area
			grabbed_offset = area.global_position - get_global_mouse_position()
			sliding_device = true
			grabbed_device.keychain.push()
			grabbed_device.velocity = Vector2()
			break
		if area is Button2D and !area.get_parent().paused:
			area.press()
			break


func reset_grab() -> void:
	sliding_device = false
	grabbed_device = null

