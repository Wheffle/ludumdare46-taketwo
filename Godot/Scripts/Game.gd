extends Node


const MenuScene = preload("res://Scenes/MainMenu.tscn")
const TabletopScene = preload("res://Scenes/Tabletop.tscn")

onready var music_menu = $MusicPlayerMenu
onready var music_tabletop = $MusicPlayerTabletop

var current_scene : Node = null
var session_score : int = 0


func _ready() -> void:
	randomize()
	change_scene(Global.SCENE.MENU)


func change_scene(scene_index : int) -> void:
	if current_scene != null:
		remove_child(current_scene)
		current_scene.queue_free()
		current_scene = null
	match(scene_index):
		Global.SCENE.MENU:
			music_tabletop.stop()
			music_menu.play()
			current_scene = MenuScene.instance()
			add_child(current_scene)
		Global.SCENE.TABLETOP:
			music_menu.stop()
			music_tabletop.play()
			current_scene = TabletopScene.instance()
			add_child(current_scene)

